package io.coderleo.scraper;

public class ApplicationInfo
{
    private static final String VERSION = "1.0.2-Alpha";

    public static String getVersion() {
        return VERSION;
    }
}
