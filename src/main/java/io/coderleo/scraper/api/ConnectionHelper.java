package io.coderleo.scraper.api;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class ConnectionHelper
{
    public static boolean ping()
    {
        try
        {
            Unirest.get("https://google.com").asString();
            return true;
        } catch (UnirestException e)
        {
            return false;
        }
    }
}
