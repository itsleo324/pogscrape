package io.coderleo.scraper.api.scrapers;

import io.coderleo.scraper.Log;
import io.coderleo.scraper.api.AbstractScraper;
import io.coderleo.scraper.api.interfaces.IScraper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

public class MCSLScraper extends AbstractScraper implements IScraper
{
    @Override
    public String getPageEndpoint()
    {
        return "page/%s/";
    }

    @Override
    public String getRootUrl()
    {
        return "http://minecraft-server-list.com/";
    }

    @Override
    public Elements getElements(Document document)
    {
        return document.getElementsByAttributeValue("name", "serverip");
    }

    @Override
    public String paginationQuerySelector()
    {
        return ".paginate.paginate-dark.pagination-wrapper ul li:last-child a";
    }

    @Override
    public void scrape()
    {
        Log.info("Scraping from MCSLScraper!");

        super.scrape();
    }

    @Override
    public void scanPage(Integer page)
    {
        Log.info("Delegating to AbstractScraper#scanPage");

        super.scanPage(page);
    }

    @Override
    public String formatFileName(String original)
    {
        return original + "_mcs-list.json";
    }

    @Override
    public String getPrettyName()
    {
        return "Minecraft Server List";
    }

    @Override
    public Document getDocument(String endpoint) throws IOException
    {
        return Jsoup.connect(getRootUrl() + endpoint)
                .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
                .header("Upgrade-Insecure-Requests", "1")
                .header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36")
                .header("Accept-Encoding", "gzip, deflate, sdch")
                .header("Cookie", "__utmt=1;")
                .get();
    }
}
