package io.coderleo.scraper.api.interfaces;

import io.coderleo.scraper.ServerState;

public interface IShodanParser
{
    public ServerState dataToServer(String data);
}
