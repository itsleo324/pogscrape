package io.coderleo.scraper.api.interfaces;

public interface ICliManager
{
    public boolean parseArguments(String[] args);

    public boolean hasArgument(String name);

    public Object getArgument(String name, Object defaultValue);

    public Object getArgument(String name);
}
