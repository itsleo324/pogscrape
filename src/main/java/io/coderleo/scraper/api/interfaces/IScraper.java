package io.coderleo.scraper.api.interfaces;

import io.coderleo.scraper.JsonFileWriter;
import io.coderleo.scraper.ServerState;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface IScraper
{
    /**
     * Get an endpoint to use when constructing pagination URLs.
     *
     * @return The endpoint
     */
    String getPageEndpoint();

    /**
     * Get the root URL to use.
     *
     * @return The root URL
     */
    String getRootUrl();

    /**
     * Get a list of matching elements for the given document.
     *
     * @param document the document
     * @return the elements
     */
    Elements getElements(Document document);

    /**
     * The query selector to use when selecting
     * the maximum pagination element.
     *
     * @return the query selector to use
     */
    String paginationQuerySelector();

    /**
     * Set the page to start from.
     *
     * @param startPage The page to start from.
     */
    void setStartPage(Integer startPage);

    /**
     * Set the maximum page.
     *
     * @param maxPage The maximum page.
     */
    void setMaxPage(Integer maxPage);

    /**
     * Get the page to start from.
     *
     * @return the page
     */
    Integer getStartPage();

    /**
     * Get the maximum page.
     *
     * @return the page
     */
    Integer getMaxPage();

    /**
     * Get the list of scanned pages.
     *
     * @return the list
     */
    ArrayList<Integer> getScannedPages();

    /**
     * Execute the given callback for the given server.
     *
     * @param server the server
     */
    void resolveCallback(ServerState server);

    /**
     * Scrape the source.
     */
    void scrape();

    /**
     * Scan a page.
     *
     * @param page The page.
     */
    void scanPage(Integer page);

    /**
     * Get a formatted filename to use when saving the JSON file.
     *
     * @param original The original file name.
     * @return The formatted filename.
     */
    String formatFileName(String original);

    /**
     * Get a document instance.
     * @param endpoint The endpoint.
     * @return The document instance
     */
    Document getDocument(String endpoint) throws IOException;

    /**
     * Determine if the version is matched by the type.
     *
     * @param type The type
     * @param version The version
     * @return true or false
     */
    boolean matches(String type, String version);

    /**
     * Determine if the page was scanned.
     *
     * @param page The page
     * @return true or false
     */
    boolean wasScanned(Integer page);

    /**
     * Add a page to the list of scraped pages.
     *
     * @param page The page
     */
    void addScannedPage(Integer page);

    /**
     * Generate an endpoint URL to use for requests.
     *
     * @param ip The IP address
     * @return The generated endpoint
     */
    String getMCApiEndpoint(String ip);

    /**
     * Determine if ip-only mode is on.
     *
     * @return true or false
     */
    boolean isIpsOnly();

    /**
     * Change whether ip-only mode is on or off.
     *
     * @param ipsOnly The new state.
     */
    void setIpsOnly(boolean ipsOnly);

    /**
     * Set the current page.
     *
     * @param currentPage The current page
     */
    void setCurrentPage(Integer currentPage);

    /**
     * Get the current filter types.
     *
     * @return the list
     */
    List<String> getFilterTypes();

    /**
     * Get the list of omitted servers.
     *
     * @return the list
     */
    List<String> getOmittedServers();

    /**
     * Get the list of online servers.
     *
     * @return the list
     */
    List<ServerState> getOnlineServers();

    /**
     * Get the current page.
     *
     * @return the current page
     */
    Integer getCurrentPage();

    /**
     * Get the file writer.
     *
     * @return The file writer
     */
    JsonFileWriter getJsonFileWriter();

    /**
     * Add an omitted server.
     *
     * @param ipAddress The IP address
     */
    void addOmittedServer(String ipAddress);

    /**
     * Add an online server.
     *
     * @param serverState The server
     */
    void addOnlineServer(ServerState serverState);

    /**
     * Save the online server list to a file.
     *
     * @throws IOException if it can't write to the file
     */
    void saveServersToFile() throws IOException;

    /**
     * Set the list of filters.
     *
     * @param filterTypes the filters
     */
    void setFilterTypes(List<String> filterTypes);

    /**
     * Start the scanning loop.
     */
    void startScanLoop() throws InterruptedException;
}
