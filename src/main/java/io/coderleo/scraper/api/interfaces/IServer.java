package io.coderleo.scraper.api.interfaces;

import java.io.IOException;

public interface IServer
{
    public enum ServerType {
        VANILLA,
        BUKKIT,
        SPIGOT,
        BEECORD,
        LILYPAD,
        WATERFALL,
        BUNGEECORD
    }

    public String getIPAddress();

    public ServerType getServerType();

    public void setIPAddress(String ipAddress);

    public void setServerType(ServerType serverType);
}
