package io.coderleo.scraper.api.cli;

import com.google.common.base.Strings;
import io.coderleo.scraper.ApplicationInfo;
import io.coderleo.scraper.Log;
import io.coderleo.scraper.api.interfaces.ICliManager;
import org.apache.commons.cli.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CliManager implements ICliManager
{
    private Map<String, Object> arguments = new HashMap<>();
    private CommandLine commandLine;

    private static CliManager ourInstance = new CliManager();

    public static CliManager getInstance() {
        return ourInstance;
    }

    private CliManager() {

    }

    public void credits() {
        credits(false);
    }

    public void credits(boolean finish) {
        if (!finish) {
            Log.info("Starting POGScraper v" + ApplicationInfo.getVersion() + ".");
            Log.info("Brought to you by CoderLeo!");
            Log.info("If you find a bug, PM CoderLeo on the Discord server.");
            Log.info("Join the POG Discord: https://discord.gg/0jXs9GxFHPKdsnrl");
            Log.info("Source code link: https://bitbucket.org/itsleo324/pogscrape");
        } else {
            Log.info("Thanks for using POGScraper!");
            Log.info("Source code link: https://bitbucket.org/itsleo324/pogscrape");
        }
    }

    @Override
    public boolean parseArguments(String[] args) {
        try {
            final CommandLineParser parser = new DefaultParser();
            final Options options = new Options();

            options.addOption(Option.builder().longOpt("type").type(String.class).hasArg().argName("type-value").build());
            options.addOption(Option.builder().longOpt("start").type(Number.class).hasArg().argName("start-value").build());
            options.addOption(Option.builder().longOpt("max-pages").type(Number.class).hasArg().argName("max-pages-value").build());
            options.addOption(Option.builder().longOpt("ips-only").type(Boolean.class).argName("ips-only-value").build());
            options.addOption(Option.builder().longOpt("automap").type(Boolean.class).argName("automap-value").build());
            options.addOption(Option.builder().longOpt("source").type(String.class).hasArg().argName("source-value").build());
            options.addOption(Option.builder().longOpt("server-source").type(String.class).hasArg().argName("server-source-value").build());

            commandLine = parser.parse(options, args);

            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public Map<String, Object> getArguments() {
        return arguments;
    }

    @Override
    public boolean hasArgument(String name) {
        return arguments.containsKey(name);
    }

    @Override
    public Object getArgument(String name, Object defaultValue) {
        if (commandLine == null) throw new IllegalStateException("commandLine was not set.");

        return commandLine.getOptionValue(name, defaultValue != null ? defaultValue.toString() : null);
    }

    @Override
    public Object getArgument(String name) {
        if (commandLine == null) throw new IllegalStateException("commandLine was not set.");

        return commandLine.getOptionValue(name);
    }

    public void dashes(int count) {
        System.out.println(Strings.repeat("-", count));
    }

    public void debug_PrintArgs() {
        arguments.forEach((key, value) -> Log.info("%s = %s", key, value));
    }

    public void autoDetect(List<String> options) {
        options.forEach(name -> {
            if (commandLine.hasOption(name)) arguments.put(name, getArgument(name));
        });
    }
}
