package io.coderleo.scraper.api.automap;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class NMapExecutor
{
    public static boolean check() {
        return Stream.of(System.getenv("PATH").split(Pattern.quote(File.pathSeparator)))
                .map(Paths::get)
                .anyMatch(path -> Files.exists(path.resolve("nmap")));
    }

    public static void run(String host) {
        if (check()) {
            //
        }
    }
}
