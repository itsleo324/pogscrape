package io.coderleo.scraper.api.automap;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.coderleo.scraper.ServerState;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MappingParser
{
    private File mappingFile;
    private Gson gson;
    private List<ServerState> currentServers;

    public MappingParser(File file) {
        this.mappingFile = file;
        this.gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        this.currentServers = new ArrayList<>();
    }

    public File getMappingFile() {
        return mappingFile;
    }

    public void read() throws IOException {
        String json = Files.toString(mappingFile, Charsets.UTF_8);
        currentServers = gson.fromJson(json, new TypeToken<List<ServerState>>(){}.getType());
    }
}
