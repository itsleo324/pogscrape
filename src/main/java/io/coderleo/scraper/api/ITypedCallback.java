package io.coderleo.scraper.api;

public interface ITypedCallback<T>
{
    void execute(T type);
}
