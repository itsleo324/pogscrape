package io.coderleo.scraper.api;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.coderleo.scraper.JsonFileWriter;
import io.coderleo.scraper.Log;
import io.coderleo.scraper.ServerState;
import io.coderleo.scraper.api.interfaces.IScraper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.util.Arrays.asList;

public abstract class AbstractScraper implements IScraper
{
    private boolean ipsOnly = false;
    private boolean verbose = false;
    private int serverCount = 0;
    private List<ServerState> onlineServers = new ArrayList<>();
    private List<String> omittedServers = new ArrayList<>();
    private List<String> filterTypes = new ArrayList<>();
    private List<ServerState> allServers = new ArrayList<>();
    private Integer currentPage = 1;
    private Integer startPage = 1;
    private Integer maxPage = null;
    private ArrayList<Integer> scannedPages = new ArrayList<>();
    private JsonFileWriter jsonFileWriter = new JsonFileWriter("");
    private ITypedCallback<ServerState> serverCallback;

    private ITypedCallback<ServerState> getServerCallback()
    {
        return serverCallback;
    }

    public void setServerCallback(ITypedCallback<ServerState> serverCallback)
    {
        this.serverCallback = serverCallback;
    }

    @Override
    public abstract String getRootUrl();

    public abstract String getPrettyName();

    @Override
    public void addOnlineServer(ServerState serverState)
    {
        onlineServers.add(serverState);
    }

    @Override
    public void addOmittedServer(String ipAddress)
    {
        omittedServers.add(ipAddress);
    }

    @Override
    public JsonFileWriter getJsonFileWriter()
    {
        return jsonFileWriter;
    }

    @Override
    public Integer getCurrentPage()
    {
        return startPage != null ? startPage : currentPage;
    }

    @Override
    public List<ServerState> getOnlineServers()
    {
        return onlineServers;
    }

    @Override
    public List<String> getOmittedServers()
    {
        return omittedServers;
    }

    private List<ServerState> getAllServers()
    {
        return allServers;
    }

    @Override
    public List<String> getFilterTypes()
    {
        return filterTypes;
    }

    @Override
    public void setCurrentPage(Integer currentPage)
    {
        this.currentPage = currentPage;
    }

    @Override
    public boolean isIpsOnly()
    {
        return ipsOnly;
    }

    @Override
    public void setIpsOnly(boolean ipsOnly)
    {
        this.ipsOnly = ipsOnly;
    }

    @Override
    public Integer getStartPage()
    {
        return startPage;
    }

    @Override
    public Integer getMaxPage()
    {
        return maxPage;
    }

    @Override
    public ArrayList<Integer> getScannedPages()
    {
        return scannedPages;
    }

    @Override
    public void resolveCallback(ServerState server)
    {
        if (getServerCallback() != null)
        {
            Log.debug("Executing server callback for IP %s", server.getIp());
            getServerCallback().execute(server);
        }
    }

    @Override
    public void setStartPage(Integer startPage)
    {
        this.startPage = startPage;
        this.currentPage = startPage;
    }

    @Override
    public void setMaxPage(Integer maxPage)
    {
        this.maxPage = maxPage;
    }

    @Override
    public void addScannedPage(Integer page)
    {
        if (!scannedPages.contains(page)) scannedPages.add(page);
    }

    public boolean isVerbose()
    {
        return verbose;
    }

    public void setVerbose(boolean verbose)
    {
        this.verbose = verbose;
    }

    @Override
    public void scrape()
    {
        try
        {
            Document document = getDocument("");
            Element maximumPageEl = document.select(paginationQuerySelector()).first();
            String href = maximumPageEl.attr("href");
            String[] hrefTokens = href.split("/");

            int minPage = getCurrentPage();
            int maxPage = (getMaxPage() != null ? getMaxPage() : Integer.parseInt(hrefTokens[2]));

            this.maxPage = maxPage;

            if (minPage > maxPage)
            {
                Log.error("Can't start from a page greater than the maximum.");
                System.exit(0);
                return;
            }

            Log.debug("Scanning minimum page (%s)", minPage);
            scanPage(minPage);
            Log.debug("Finished scanning minimum page.");

            Log.debug("Starting scan loop from AbstractScraper#scrape");
            startScanLoop();

            Log.info("Done!");

            Log.debug("Saving online servers to file.");
            saveServersToFile();
            Log.debug("Finished saving online servers.");
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void scanPage(Integer page)
    {
        try
        {
            Document document = getDocument(String.format(getPageEndpoint(), page));
            Elements elements = getElements(document);

            Log.debug("Found %s elements to scan.", elements.size());
            scanElements(elements);
            Log.debug("Finished scanning DOM elements.");

            addScannedPage(page);
            currentPage++;
        } catch (IOException | UnirestException e)
        {
            Log.error("An error occurred while trying to scrape the page.");
            e.printStackTrace();
        }
    }

    @Override
    public abstract Document getDocument(String endpoint) throws IOException;

    @Override
    public String getMCApiEndpoint(String ip)
    {
        return String.format("https://us.mc-api.net/v3/server/info/%s/json", ip);
    }

    @Override
    public void saveServersToFile() throws IOException
    {
        final String fileName = formatFileName(String.format("output/%s_%s",
                new SimpleDateFormat("yyyy_MM_dd_hh:mm:ss").format(new Date()),
                filterTypes.size() > 0 ? String.join("_", filterTypes).toUpperCase() : "all".toUpperCase()
        ));

        Log.info("Writing server info to file: %s", fileName);
        getJsonFileWriter().setFileName(fileName);
        getJsonFileWriter().write(onlineServers);
    }

    @Override
    public void setFilterTypes(List<String> filterTypes)
    {
        this.filterTypes = filterTypes;
    }

    @Override
    public boolean wasScanned(Integer page)
    {
        return scannedPages.contains(page);
    }

    @Override
    public void startScanLoop()
    {
        final Boolean isCurrentPage = Objects.equals(startPage, maxPage);

        if (isCurrentPage)
        {
            Log.debug("Start page (%s) is equal to maximum page (%s), no need for loop.", startPage, maxPage);
            return;
        }

        Log.debug("Scan loop is starting from class: %s", getClass().getCanonicalName());
        Log.debug("Current page: %s", currentPage);
        Log.debug("Maximum page: %s", maxPage);

        while (currentPage <= maxPage && !wasScanned(currentPage))
        {
            final int realPage = currentPage;
            Log.debug("Scanning page %s!", currentPage);

            scanPage(currentPage);

            Log.info("Finished scanning page %s!", realPage);
        }
    }

    @Override
    public boolean matches(String type, String version)
    {
        switch (type)
        {
            case "bungeecord": // BungeeCord servers
            case "bungee":
                return version.matches("(?i).*bungee.*");
            case "lilypad": // Lilypad (guessing on version string)
                return version.matches("(?i).*lily.*");
            case "spigot": // Spigot servers
                return version.matches("(?i).*spigot.*");
            case "waterfall":
                return version.matches("(?i).*waterfall.*");
        }

        return true;
    }

    private String predictType(String version)
    {
        final List<String> types = asList("bungeecord", "bungee", "lilypad", "spigot", "waterfall");
        for (String type : types)
        {
            if (matches(type, version)) return StringUtils.capitalize(type.toLowerCase());
        }

        return "N/A";
    }

    public int getServerCount()
    {
        return serverCount;
    }

    /**
     * Scan a list of elements.
     *
     * @param elements The elements
     * @throws UnirestException
     */
    private void scanElements(Elements elements) throws UnirestException
    {
        elements.forEach(this::scanElement);
    }

    /**
     * Scan an element.
     *
     * @param element The element
     */
    private void scanElement(Element element)
    {
        try
        {
            String ip = element.tagName().equals("input") ? element.val() : (element.isBlock() ? element.text() : "");
            if (this.isIpsOnly() && !(new InetAddressValidator()).isValid(ip))
            {
                addOmittedServer(ip);
                return;
            }

            serverCount++;

            HttpResponse<JsonNode> response = Unirest.get(getMCApiEndpoint(ip)).asJson();
            if (getFilterTypes().size() == 0) Log.info("Found IP: %s", ip);

            if (response.getStatus() != 200)
            {
                Log.warn("Failed to get info for IP: " + ip + " (status code: " + response.getStatus() + ")");
                return;
            }

            JsonNode body = response.getBody();
            JSONObject object = body.getObject();

            if ((!object.has("version")) || !(object.getBoolean("online")) || !(object.has("players"))) {
                Log.warn("Failed to get version for IP: " + ip + "; probably offline!");
                return;
            }

            JSONObject version = object.getJSONObject("version");
            JSONObject players = object.getJSONObject("players");
            String versionString = version.getString("name");
            Integer onlinePlayers = players.getInt("online");
            Integer maxPlayers = players.getInt("max");

            final ServerState serverState = buildServerObject(ip, onlinePlayers, maxPlayers, versionString != null && !Objects.equals(versionString, "") ? versionString : "N/A");

            getAllServers().add(serverState);

            if (getFilterTypes().size() > 0) {
                getFilterTypes().forEach(type -> {
                    if (matches(type, versionString)) {
                        Log.info("Found matching IP: %s", ip);
                        Log.info("Looks like this server is a %s server! Just what you wanted :)", StringUtils.capitalize(type.toLowerCase()));

                        serverState.setType(StringUtils.capitalize(type.toLowerCase()));
                        addOnlineServer(serverState);
                        resolveCallback(serverState);
                    } else {
                        addOmittedServer(ip);
                    }
                });
            } else {
                Log.info("No preferred server type set, so showing it anyway: %s", ip);
                serverState.setType(predictType(versionString));
                addOnlineServer(serverState);
                resolveCallback(serverState);
            }
        } catch (JSONException | UnirestException e)
        {
            Log.error("An error occurred while trying to scrape this source.");
            e.printStackTrace();
        }
    }

    private ServerState buildServerObject(
            String ip,
            Integer players,
            Integer maxPlayers,
            String version
    )
    {
        ServerState serverState = new ServerState();
        serverState.setIp(ip);
        serverState.setPlayers(players);
        serverState.setMaxPlayers(maxPlayers);
        serverState.setVersion(StringUtils.capitalize(version.toLowerCase()));

        if (ip.split(":").length == 2) serverState.setPort(Integer.parseInt(ip.split(":")[1]));

        return serverState;
    }

    @Override
    public String toString()
    {
        return getPrettyName();
    }
}
