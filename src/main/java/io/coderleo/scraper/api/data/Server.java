package io.coderleo.scraper.api.data;

import io.coderleo.scraper.api.interfaces.IServer;

public class Server implements IServer
{
    private String ipAddress;
    private ServerType serverType;

    public Server(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Server(String ipAddress, ServerType serverType) {
        this.ipAddress = ipAddress;
        this.serverType = serverType;
    }

    @Override
    public String getIPAddress() {
        return ipAddress;
    }

    @Override
    public ServerType getServerType() {
        return serverType;
    }

    @Override
    public void setIPAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Override
    public void setServerType(ServerType serverType) {
        this.serverType = serverType;
    }
}
