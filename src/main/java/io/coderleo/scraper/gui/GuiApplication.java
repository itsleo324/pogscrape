package io.coderleo.scraper.gui;

import io.coderleo.scraper.Log;
import io.coderleo.scraper.ServerState;
import io.coderleo.scraper.api.AbstractScraper;
import io.coderleo.scraper.api.scrapers.MCSLScraper;
import io.coderleo.scraper.api.scrapers.MinecraftServersScraper;
import io.coderleo.scraper.gui.util.Settings;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

public class GuiApplication extends Application
{
    // Settings-related properties
    private AbstractScraper currentScraper = new MCSLScraper();

    // UI elements
    private ObservableList<ServerState> servers = FXCollections.observableArrayList();
    private ObservableList<AbstractScraper> scrapers = FXCollections.observableArrayList();

    // UI elements
    private Button scrapeButton;
    private TableView<ServerState> tableView;
    private ComboBox<AbstractScraper> scraperComboBox = new ComboBox<>();
    private Stage primaryStage;

    private HBox header;

    // Settings UI
    private Button buttonSave;
    private TextField startingPage;
    private TextField maximumPage;
    private List<String> activeFilters = new ArrayList<>();

    private SettingsManager settingsManager = SettingsManager.getInstance();
    private Settings settings = settingsManager.getSettings();

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        setupScraperInfo();

        Scene scene = new Scene(createBorderPane(), 775, 420);
        scene.getStylesheets().add(GuiApplication.class.getClassLoader().getResource("styles.css").toExternalForm());

        primaryStage.setTitle("POGScraper GUI");
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();

        this.primaryStage = primaryStage;
    }

    @Override
    public void stop()
    {
        Log.info("Closing scraper...");
        System.exit(0);
    }

    /**
     * Set up the scraper properties.
     */
    private void setupScraperInfo()
    {
        scrapers.addAll(new MCSLScraper(), new MinecraftServersScraper());
        activeFilters = settings.getFilterTypes();

        if (settings.getPreferredScraper() != null)
        {
            Class<?> scraperClass;

            try
            {
                scraperClass = Class.forName(settings.getPreferredScraper());
                currentScraper = (AbstractScraper) scraperClass.newInstance();
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e)
            {
                e.printStackTrace();
            }
        }
    }

    private GridPane createCenterPane()
    {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20));

        grid.getChildren().add(createTable());

        return grid;
    }

    private TableView createTable()
    {
        tableView = new TableView<>();

        TableColumn<ServerState, String> ipAddressCol = new TableColumn<>("IP Address");
        ipAddressCol.setCellValueFactory(new PropertyValueFactory<>("ip"));

        TableColumn<ServerState, Integer> playersOnlineCol = new TableColumn<>("Players Online");
        playersOnlineCol.setCellValueFactory(new PropertyValueFactory<>("players"));

        TableColumn<ServerState, String> versionCol = new TableColumn<>("Version");
        versionCol.setCellValueFactory(new PropertyValueFactory<>("version"));

        ipAddressCol.prefWidthProperty().bind(tableView.widthProperty().divide(3));
        playersOnlineCol.prefWidthProperty().bind(tableView.widthProperty().divide(3));
        versionCol.prefWidthProperty().bind(tableView.widthProperty().divide(3));

        versionCol.setCellFactory(param -> new TableCell<ServerState, String>()
        {
            @Override
            protected void updateItem(String item, boolean empty)
            {
                super.updateItem(item, empty);

                if (item == null || empty)
                {
                    setText(null);
                    setStyle("");
                } else
                {
                    setText(item);
                    setTooltip(new Tooltip("Version: " + item));
                }
            }
        });

        Label label = new Label("Looks like you haven't scraped anything.\nMaybe you should do that!");
        label.setTextAlignment(TextAlignment.CENTER);

        tableView.setPlaceholder(label);
        tableView.getColumns().addAll(Arrays.asList(ipAddressCol, playersOnlineCol, versionCol));

        GridPane.setHgrow(tableView, Priority.ALWAYS);
        GridPane.setVgrow(tableView, Priority.ALWAYS);

        tableView.setItems(servers);
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        return tableView;
    }

    private BorderPane createBorderPane()
    {
        BorderPane pane = new BorderPane();
        pane.setStyle("-fx-background-color: lightgray;");

        pane.setTop(createHeader());
        pane.setCenter(createCenterPane());
        pane.setBottom(createFooter());

        return pane;
    }

    private HBox createHeader()
    {
        DropShadow ds = new DropShadow();
        ds.setColor(Color.DARKGRAY);
        ds.setOffsetY(5.5);

        header = new HBox();
        header.setSpacing(10);
        header.setPadding(new Insets(12.5));
        header.setStyle("-fx-background-color: #18b420; -fx-border-color: transparent; -fx-border-width: -1;");

        Text title = new Text("POGScraper");
        title.setFont(Font.font("Lucida Grande", FontWeight.BOLD, FontPosture.REGULAR, 22));
        title.setFill(Color.BLACK);

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        header.setEffect(ds);
        header.getChildren().addAll(title, spacer);

        addHeaderButtons(header);

        // ComboBox setup
        scraperComboBox.setItems(scrapers);
        scraperComboBox.setStyle("-fx-focus-color: transparent; -fx-faint-focus-color: transparent; -fx-border-insets: -1; -fx-border-color: transparent; -fx-border-width: -1; -fx-background-color: #eee;");

        Log.debug("Selected preferred scraper.");
        scraperComboBox.getSelectionModel().select(currentScraper);

        scraperComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            this.currentScraper = newValue;
            Log.debug("Selected new scraper: %s", newValue);
            Log.debug("Updating preferred scraper.");

            try
            {
                settings.setPreferredScraper(newValue.getClass().getCanonicalName());
                settingsManager.save(settings);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        });

        header.getChildren().add(scraperComboBox);

        return header;
    }

    private void addHeaderButtons(HBox header)
    {
        scrapeButton = new Button("Scrape");
        scrapeButton.setStyle("-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 3.5, 0.0 , 0 , 1.5 ); -fx-focus-color: transparent; -fx-faint-focus-color: transparent; -fx-border-insets: -1; -fx-border-color: transparent; -fx-border-width: -1; -fx-background-color: #eee;");
        scrapeButton.setOnMouseClicked(this::scrape);

        Button settingsButton = new Button("Settings");
        settingsButton.setStyle("-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 3.5, 0.0 , 0 , 1.5 ); -fx-focus-color: transparent; -fx-faint-focus-color: transparent; -fx-border-insets: -1; -fx-border-color: transparent; -fx-border-width: -1; -fx-background-color: #eee;");
        settingsButton.setOnMouseClicked(this::onSettingsClicked);

        header.getChildren().addAll(scrapeButton, settingsButton);
//        header.getChildren().forEach(node -> node.addEventHandler(
//                MouseEvent.MOUSE_CLICKED, event -> node.setOnMouseClicked((e) -> header.requestFocus())
//        ));

        Platform.runLater(header::requestFocus);
    }

    private HBox createFooter()
    {
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(15));
        hBox.setSpacing(15);
        hBox.setStyle("-fx-background-color: white;");
        hBox.setAlignment(Pos.CENTER_LEFT);

        Text text = new Text("Programmed by CoderLeo.");
        text.setFont(Font.font("Lucida Grande", FontWeight.BOLD, FontPosture.REGULAR, 13.5));

        Image image = new Image(GuiApplication.class.getClassLoader().getResourceAsStream("POGLogo.jpg"));
        ImageView imageView = new ImageView();
        imageView.setFitHeight(50);
        imageView.setFitWidth(50);
        imageView.setPreserveRatio(true);
        imageView.setImage(image);

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        Hyperlink sourceLink = new Hyperlink("Source Code");
        sourceLink.setStyle("-fx-border-color: transparent; -fx-border-width: -1; -fx-border-insets: 0, 0, 0, 0;");
        sourceLink.setOnMouseClicked(event -> getHostServices().showDocument("https://bitbucket.org/itsleo324/pogscrape"));

        hBox.getChildren().addAll(imageView, text, spacer, sourceLink);

        return hBox;
    }

    private void scrape(MouseEvent e)
    {
        header.requestFocus();

        currentScraper.setVerbose(e.isShiftDown());

        ScrapeWorker worker = new ScrapeWorker();
        worker.start();

        header.requestFocus();
    }

    private void onSettingsClicked(MouseEvent e)
    {
        header.requestFocus();

        final BooleanProperty firstTime = new SimpleBooleanProperty(true);

        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        dialog.setTitle("POGScraper - Settings");
        dialog.setResizable(false);

        BorderPane border = new BorderPane();
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(20));
        hBox.setSpacing(10);
        hBox.setStyle("-fx-background-color: #336699;");

        Text text = new Text("Settings");
        text.setFont(Font.font("Lucida Grande", FontWeight.BOLD, FontPosture.REGULAR, 22));
        text.setFill(Color.BLACK);
        text.setStroke(Color.BLACK);

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        StackPane stack = new StackPane();
        Rectangle closeIcon = new Rectangle(30.0, 30.0);
        closeIcon.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE,
                new Stop(0, Color.web("#FF0000")),
                new Stop(0.5, Color.web("#FF00CC")),
                new Stop(1, Color.web("#FF0000AA"))
        ));
        closeIcon.setStroke(Color.web("#D0E6FA"));
        closeIcon.setArcHeight(3.5);
        closeIcon.setArcWidth(3.5);

        Text closeText = new Text("X");
        closeText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        closeText.setFill(Color.WHITE);
        closeText.setStroke(Color.web("#FF0000"));

        stack.setOnMouseClicked(event -> {
            Log.debug("Close button pressed");
            dialog.close();
        });

        stack.getChildren().addAll(closeIcon, closeText);
        stack.setAlignment(Pos.CENTER_RIGHT);     // Right-justify nodes in stack
        StackPane.setMargin(closeText, new Insets(0, 10, 0, 0)); // Center "X"

        HBox.setHgrow(stack, Priority.ALWAYS);    // Give stack any extra space

        hBox.getChildren().addAll(text, stack);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20));

        startingPage = new TextField()
        {
            {
                addEventFilter(KeyEvent.KEY_TYPED, event -> {
                    char[] chars;

                    if ((chars = event.getCharacter().toCharArray()).length > 0)
                    {
                        char ch = chars[chars.length - 1];
                        if (chars[0] == ch && ch == '0')
                        {
                            Log.error("Invalid number typed.");
                            event.consume();
                        }

                        if (!(ch >= '0' && ch <= '9'))
                        {
                            Log.error("Invalid number typed.");
                            event.consume();
                        }
                    }
                });
            }
        };

        maximumPage = new TextField()
        {
            {
                addEventFilter(KeyEvent.KEY_TYPED, event -> {
                    char[] chars;

                    if ((chars = event.getCharacter().toCharArray()).length > 0)
                    {
                        char ch = chars[chars.length - 1];
                        if (chars[0] == ch && ch == '0')
                        {
                            Log.error("Invalid number typed.");
                            event.consume();
                        }

                        if (!(ch >= '0' && ch <= '9'))
                        {
                            Log.error("Invalid number typed.");
                            event.consume();
                        }
                    }
                });
            }
        };

        startingPage.setText(settings.getStartingPage() != null ? settings.getStartingPage().toString() : Integer.toString(1));
        maximumPage.setText(settings.getMaxPages() != null ? settings.getMaxPages().toString() : null);

        grid.add(new Label("Starting page: "), 0, 0);
        grid.add(startingPage, 1, 0);

        grid.add(new Label("Maximum page: "), 0, 1);
        grid.add(maximumPage, 1, 1);

        grid.add(new Label("Filters: "), 0, 2);
        CheckBox bungee = new CheckBox("BungeeCord");
        CheckBox lilyPad = new CheckBox("LilyPad");
        CheckBox spigot = new CheckBox("Spigot");
        CheckBox waterfall = new CheckBox("Waterfall");

        Arrays.asList(bungee, lilyPad, spigot, waterfall).forEach((checkbox) -> {
            checkbox.setSelected(activeFilters.contains(checkbox.getText().toLowerCase()));
            checkbox.setOnMouseClicked(event -> toggleFilter(checkbox.getText().toLowerCase()));
        });

        grid.add(bungee, 1, 2);
        grid.add(lilyPad, 1, 3);
        grid.add(spigot, 1, 4);
        grid.add(waterfall, 1, 5);

        startingPage.setPromptText("Enter the page to start from...");
        startingPage.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue && firstTime.get())
            {
                grid.requestFocus(); // Delegate the focus to container
                firstTime.setValue(false); // Variable value changed for future references
            }
        });

        ChangeListener<String> changeListener = (observable, oldValue, newValue) -> {
            Log.info("validating start page: %s", newValue);

            buttonSave.setDisable(newValue.isEmpty());
        };

        grid.add(new Text("More stuff coming soon\u2122"), 0, 6);

        border.setTop(hBox);
        border.setCenter(addSettingsAnchor(grid, dialog));

        startingPage.textProperty().addListener(changeListener);
        changeListener.changed(startingPage.textProperty(), null, startingPage.getText());

//        startingPage.textProperty().addListener(changeListener);
        buttonSave.setOnMouseClicked(event -> saveSettings(dialog));

        Scene scene = new Scene(border, 775, 420); // these numbers are not supposed to mean anything.
        dialog.setScene(scene);
        dialog.show();
    }

    private AnchorPane addSettingsAnchor(GridPane grid, Stage dialog)
    {
        AnchorPane anchorpane = new AnchorPane();
        buttonSave = new Button("Save");
        Button buttonCancel = new Button("Cancel");
        Button buttonApply = new Button("Apply");

        buttonApply.setOnMouseClicked(event -> saveSettings(dialog, false));
        buttonCancel.setOnMouseClicked(event -> dialog.close());

        HBox hb = new HBox();
        hb.setPadding(new Insets(0, 10, 10, 10));
        hb.setSpacing(10);
        hb.getChildren().addAll(buttonApply, buttonSave, buttonCancel);

        anchorpane.getChildren().addAll(grid, hb);
        AnchorPane.setBottomAnchor(hb, 8.0);
        AnchorPane.setRightAnchor(hb, 5.0);
        AnchorPane.setTopAnchor(grid, 10.0);

        return anchorpane;
    }

    private void saveSettings(Stage dialog)
    {
        saveSettings(dialog, true);
    }

    private void saveSettings(Stage dialog, boolean close)
    {
        Log.debug("Save button pressed");

        Integer startPage = 1;
        Integer maxPage = null;
//
        if (startingPage.getText() != null && !startingPage.getText().isEmpty())
        {
            startPage = Integer.parseInt(startingPage.getText());
        }

        if (maximumPage.getText() != null && !maximumPage.getText().isEmpty())
        {
            maxPage = Integer.parseInt(maximumPage.getText());
        }

        settings.setStartingPage(startPage);
        settings.setMaxPages(maxPage);
        settings.setFilterTypes(activeFilters);

        try
        {
            SettingsManager.getInstance().save(settings);
            if (close) dialog.close();
        } catch (IOException e)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Internal Error");
            alert.setHeaderText("An error occurred while trying to save your settings.");
            alert.setContentText(e.getMessage());

            // Create the expandable exception info
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String exceptionText = sw.toString();

            Label label = new Label("Exception stacktrace:");

            TextArea textArea = new TextArea(exceptionText);
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(label, 0, 0);
            expContent.add(textArea, 0, 1);

            // Set expandable content as the exception info
            alert.getDialogPane().setExpandableContent(expContent);

            alert.showAndWait();
        }
    }

    private class ScrapeWorker extends Thread implements Runnable
    {
        @Override
        public void run()
        {
            if (currentScraper.isVerbose()) Log.debug("run() executed from ScrapeWorker");

            servers.clear();
            Log.debug("Cleared server list");

            Platform.runLater(() -> {
                scrapeButton.setDisable(true);
                scrapeButton.setText("Scraping...");
            });

            currentScraper.setStartPage(settings.getStartingPage() != null ? settings.getStartingPage() : 1);
            currentScraper.setMaxPage(settings.getMaxPages());
            currentScraper.setFilterTypes(activeFilters);
            currentScraper.setServerCallback(server -> {
                Log.debug("Got server: %s", server);
                Log.debug("IP was %s", server.getIp());
                Log.debug("Server list = %s", servers.getClass().getCanonicalName());

                servers.add(server);

                Platform.runLater(() -> tableView.sort());
            });

            currentScraper.scrape();

            Platform.runLater(() -> {
                scrapeButton.setDisable(false);
                scrapeButton.setText("Scrape");
            });
        }
    }

    private void toggleFilter(String name)
    {
        if (activeFilters.contains(name)) activeFilters.remove(name);
        else activeFilters.add(name);
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
