package io.coderleo.scraper.gui;

public class SessionSettings
{
    private static SessionSettings ourInstance = new SessionSettings();

    public static SessionSettings getInstance()
    {
        return ourInstance;
    }

    private SessionSettings()
    {
    }
}
