package io.coderleo.scraper.gui;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.coderleo.scraper.Log;
import io.coderleo.scraper.api.scrapers.MCSLScraper;
import io.coderleo.scraper.gui.util.Settings;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

class SettingsManager
{
    // instance properties
    private Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
    private File settingsFile = new File(System.getProperty("user.home") + File.separator + ".pogscraper-ui" + File.separator + "settings.json");
    private Settings settings;

    // static instance
    private static SettingsManager ourInstance = new SettingsManager();

    static SettingsManager getInstance()
    {
        return ourInstance;
    }

    private SettingsManager()
    {
        load();
    }

    private void load()
    {
        try {
            if (!settingsFile.exists()) {
                Log.debug("Creating settings file");

                settingsFile.getParentFile().mkdirs();
                settingsFile.createNewFile();

                settings = new Settings();
                settings.setPreferredScraper(MCSLScraper.class.getCanonicalName());

                save(settings);
            }

            Log.debug("Reading from settings file in %s", settingsFile.getCanonicalPath());
            settings = gson.fromJson(Files.toString(settingsFile, Charsets.UTF_8), Settings.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void save(Settings settings) throws IOException
    {
        try (FileOutputStream fileOutputStream = new FileOutputStream(settingsFile)) {
            fileOutputStream.write(gson.toJson(settings).getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File getSettingsFile()
    {
        return settingsFile;
    }

    Settings getSettings()
    {
        return settings;
    }
}
