package io.coderleo.scraper.gui.util;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Settings
{
    @SerializedName(value = "Filters", alternate = {"filters"})
    private List<String> filterTypes = new ArrayList<>();

    @SerializedName(value = "MaxPages", alternate = {"maxPages"})
    private Integer maxPages = null;

    @SerializedName(value = "StartPage", alternate = {"startPage"})
    private Integer startingPage = 1;

    @SerializedName(value = "PreferredScraper", alternate = {"preferredScraper"})
    private String preferredScraper;

    public List<String> getFilterTypes()
    {
        return filterTypes;
    }

    public void setFilterTypes(List<String> filterTypes)
    {
        this.filterTypes = filterTypes;
    }

    public Integer getMaxPages()
    {
        return maxPages;
    }

    public void setMaxPages(Integer maxPages)
    {
        this.maxPages = maxPages;
    }

    public Integer getStartingPage()
    {
        return startingPage;
    }

    public void setStartingPage(Integer startingPage)
    {
        this.startingPage = startingPage;
    }

    public String getPreferredScraper()
    {
        return preferredScraper;
    }

    public void setPreferredScraper(String preferredScraper)
    {
        this.preferredScraper = preferredScraper;
    }
}
