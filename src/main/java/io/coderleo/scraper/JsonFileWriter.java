package io.coderleo.scraper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class JsonFileWriter
{
    private String fileName;

    public JsonFileWriter(String file)
    {
        fileName = file;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public void write(List<ServerState> servers) throws IOException
    {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String s = gson.toJson(servers, new TypeToken<List<ServerState>>() {}.getType());

        File file = new File(fileName);
        if (file.getParentFile() != null) {
            Log.debug("making directories if necessary");
            file.getParentFile().mkdirs();
        }

        try (FileOutputStream fileOutputStream = new FileOutputStream(file))
        {
            fileOutputStream.write(s.getBytes());
            fileOutputStream.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
