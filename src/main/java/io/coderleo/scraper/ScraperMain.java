package io.coderleo.scraper;

import io.coderleo.scraper.api.AbstractScraper;
import io.coderleo.scraper.api.ConnectionHelper;
import io.coderleo.scraper.api.cli.CliManager;
import io.coderleo.scraper.api.scrapers.MCSLScraper;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

public class ScraperMain
{
    private static String outFormat = "[ DATA ] %-32s  %-8s  %-15s\n";
    private static CliManager cliManager = CliManager.getInstance();

    public static void main(String[] args) throws InterruptedException {
        // TODO: 7/3/16 more cleanup

        Log.setup();
        if (!ConnectionHelper.ping()) {
            Log.error("POGScraper requires an internet connection.");
            System.exit(0);
            return;
        }

        // launch credits
        cliManager.credits();
        cliManager.dashes(52);

        // cli args
        cliManager.parseArguments(args);
        cliManager.autoDetect(Arrays.asList(
                "type",
                "start",
                "max-pages",
                "ips-only",
                "automap",
                "server-source",
                "source"
        ));
        String scraper = MCSLScraper.class.getCanonicalName();
        Class<?> scraperInstance;

        Object type = cliManager.getArgument("type");
        Object startPage = cliManager.getArgument("start");
        Object maxPages = cliManager.getArgument("max-pages");
        Boolean ipsOnly = Boolean.parseBoolean(cliManager.getArgument("ipsOnly", false).toString());
//        Boolean autoMap = Boolean.parseBoolean(cliManager.getArgument("automap", false).toString());
//        String serverSource = (String) cliManager.getArgument("server-source", "");

        if (cliManager.hasArgument("source")) scraper = cliManager.getArgument("source").toString();

        try {
            scraperInstance = Class.forName(scraper);
            AbstractScraper abstractScraper = (AbstractScraper) scraperInstance.newInstance();
            if (type != null) abstractScraper.setFilterTypes(Arrays.asList(type.toString().split(",")));
            if (maxPages != null) abstractScraper.setMaxPage(Integer.parseInt(maxPages.toString()));
            if (startPage != null) abstractScraper.setStartPage(Integer.parseInt(startPage.toString()));

            abstractScraper.setIpsOnly(ipsOnly);

            Log.info("Scraping from class: %s", scraper);
            if (type != null) Log.info("Accepted types: %s", StringUtils.join(type.toString().split(","), ", "));
            if (startPage != null) Log.info("Starting from page %s", startPage);
            if (maxPages != null) Log.info("Maximum pages: %s", maxPages);

            if ((startPage != null && maxPages != null) && Integer.parseInt(startPage.toString()) > Integer.parseInt(maxPages.toString())) {
                Log.error("Can't start from a page greater than the maximum.");
                System.exit(0);
                return;
            }

            Thread.sleep(1250);

            abstractScraper.scrape();

            Log.success("Done!");
            Log.info("Here's a little report for you.");
            final List<ServerState> onlineServers = abstractScraper.getOnlineServers();
            final int total = abstractScraper.getServerCount();
            System.out.format("[      ] %-32s  %-8s  %-15s\n", "IP", "Players", "Version");
            onlineServers.forEach(server -> System.out.format(outFormat, new Object[] {
                    server.getIp() + ":" + server.getPort(),
                    Integer.valueOf(server.getPlayers()),
                    server.getVersion()
            }));

            Log.info("Out of %s server%s, %s %s online.", total, total != 1 ? "s" : "", onlineServers.size(), onlineServers.size() != 1 ? "were" : "was");
        } catch (ClassNotFoundException e) {
            Log.error("Scraper class not found: %s", scraper);
            System.exit(1);
        } catch (InstantiationException e) {
            Log.error("Couldn't instantiate scraper: %s", scraper);
            e.printStackTrace();
            System.exit(1);
        } catch (IllegalAccessException e) {
            Log.error("Failed to access scraper: %s", scraper);
            e.printStackTrace();
            System.exit(1);
        } catch (NumberFormatException e) {
            Log.error("Invalid number provided.");
            System.exit(1);
        }

        Thread.sleep(950);

        // end credits
        cliManager.dashes(52);

        cliManager.credits(true);
    }
}