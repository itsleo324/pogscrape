package io.coderleo.scraper.util;

import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Strings;

public class Table {

    private class Index {

        int _row, _column;

        public Index (int r, int c) {
            _row= r;
            _column = c;
        }

        @Override
        public boolean equals (Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Index other= (Index) obj;
            if (_column != other._column)
                return false;
            if (_row != other._row)
                return false;
            return true;
        }

        @Override
        public int hashCode () {
            final int prime= 31;
            int result= 1;
            result= prime * result + _column;
            result= prime * result + _row;
            return result;
        }
    }

    Map<Index, String> _strings= new HashMap<>();
    Map<Integer, Integer> _columnSizes = new HashMap<>();

    int _numRows= 0;
    int _numColumns= 0;

    public void addString (int row, int column, String content) {
        _numRows= Math.max(_numRows, row + 1);
        _numColumns= Math.max(_numColumns, column + 1);

        Index index= new Index(row, column);
        _strings.put(index, content);

        setMaxColumnSize(column, content);
    }

    private void setMaxColumnSize (int column, String content) {
        int size= content.length();
        Integer currentSize= _columnSizes.get(column);
        if (currentSize == null || currentSize < size) {
            _columnSizes.put(column, size);
        }
    }

    public int getColumSize (int column) {
        Integer size= _columnSizes.get(column);
        if (size == null) {
            return 0;
        } else {
            return size;
        }
    }

    public String getString (int row, int column) {
        Index index= new Index(row, column);
        String string= _strings.get(index);
        if (string == null) {
            return "";
        } else {
            return string;
        }
    }

    public String getTableAsString (int padding) {
        String out= "";
        for (int r= 0; r < _numRows; r++) {
            for (int c= 0; c < _numColumns; c++) {
                int columnSize= getColumSize(c);
                String content= getString(r, c);
                int pad= c == _numColumns - 1 ? 0 : padding;
                out+= Strings.padEnd(content, columnSize + pad, ' ');
            }
            if (r < _numRows - 1) {
                out+= "\n";
            }
        }
        return out;
    }

    @Override
    public String toString () {
        return getTableAsString(1);
    }

}