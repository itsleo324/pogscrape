package io.coderleo.scraper;

import com.google.common.base.Strings;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.coderleo.scraper.api.cli.CliManager;
import org.fusesource.jansi.AnsiConsole;

import java.io.*;
import java.util.Arrays;

public class ScraperMainOld
{
//    private static ArrayList<ServerState> onlineServers = new ArrayList<>();
//    private static ArrayList<String> validTypes = new ArrayList<String>() {{
//        add("bungeecord");
//        add("lilypad");
//        add("spigot");
//        add("waterfall");
//    }};

    private static CliManager cliManager = CliManager.getInstance();
//    private static volatile Integer currentPage = 1;
//    private static Integer maxPages = null;
//    private static volatile Boolean isFinished = false;
//    private static List<String> serverType = null;
//    private static Boolean ipsOnly = false;
//    private static Boolean automap = false;
//    private static String serverSourceValue = null;

//    private static JsonFileWriter fileWriter = new JsonFileWriter("");

    public static void main(String[] args) throws IOException, UnirestException, InterruptedException {
        AnsiConsole.systemInstall();

        try {
            Unirest.get("https://google.com").asString();
        } catch (UnirestException e) {
            Log.error("POGScrape requires an internet connection.");
            System.exit(0);
            return;
        }

        Log.info("Starting POGScraper v" + ApplicationInfo.getVersion() + ".");
        Log.info("Brought to you by CoderLeo!");
        Log.info("If you find a bug, PM CoderLeo on the Discord server.");
        Log.info("Join the POG Discord: https://discord.gg/0jXs9GxFHPKdsnrl");
        Log.info("Source code link: https://bitbucket.org/itsleo324/pogscrape");

        cliManager.parseArguments(args);
        cliManager.autoDetect(Arrays.asList(
                "type",
                "start",
                "max-pages",
                "ips-only",
                "automap",
                "server-source"
        ));

        cliManager.getArguments().forEach((key, value) -> System.out.println(key + ": " + value));

        System.out.println(Strings.repeat("-", 52));

        Thread.sleep(1500);

//        if (!automap) { // regular execution
//            ScanThread scanThread = new ScanThread(serverType);
//            scanThread.start();
//
//            while (!isFinished) {
//                nothing();
//            }
//
//            Table table = new Table();
//            table.addString(0, 0, Strings.padStart("IP", 7, ' '));
//            table.addString(0, 1, Strings.padStart("Players", 15, ' '));
//            table.addString(0, 2, Strings.padStart("Version", 23, ' '));
//            System.out.println(Strings.repeat("-", 72));
//
//            int currentRow = 1;
//
//            for (ServerState serverState : onlineServers) {
//                table.addString(currentRow, 0, serverState.getIp());
//                table.addString(currentRow, 1, Strings.padStart(serverState.getPlayers() + "/" + serverState.getMaxPlayers(), 16, ' '));
//                table.addString(currentRow, 2, Strings.padStart(serverState.getVersion(), 36, ' '));
//                currentRow++;
//            }
//
//            if (ipsOnly && !scanThread.omittedAddresses.isEmpty()) {
//                Log.warn("Number of servers omitted (IPv4 only mode): " + Integer.toString(scanThread.omittedAddresses.size()));
//            }
//
//            if (onlineServers.size() > 0) {
//                System.out.println(table.toString());
//            }
//
//            Log.info("Finished scraping! Online servers: " + onlineServers.size());
//            Log.info("Omitted servers (irrelevant version): " + scanThread.omittedServers.size());
//        } else {
//            // automap
//            Gson gson = new GsonBuilder().setPrettyPrinting().create();
//            Log.info("Processing mapping file...");
//            File file = new File(serverSourceValue);
//            String json = Files.toString(file, Charsets.UTF_8);
//
//            List<ServerState> servers = gson.fromJson(json, new TypeToken<List<ServerState>>(){}.getType());
//
//            servers.forEach(server -> Log.info("Found server from file - IP %s, version %s", server.getIp(), server.getVersion()));
//        }

        CliManager.getInstance().credits(true);
        System.exit(0);
    }
//    private static void checkFile(String path) {
//        if (!new File(path).exists()) {
//            Log.error("Invalid file.");
//            System.exit(0);
//        } else {
//            Log.success("Found a file by that name!");
//        }
//    }
//
//    private static void nothing() {
//        //
//    }
//
//    private static class ScanThread extends Thread {
//        private List<String> type;
//        private String fileName;
//        private volatile boolean hasCompleted = false;
//        public ArrayList<String> omittedAddresses = new ArrayList<>();
//        public ArrayList<String> omittedServers = new ArrayList<>();
//        private ArrayList<Integer> scannedPages = new ArrayList<>();
//        private int retries = 0;
////        private int hostRetries = 0;
////        private volatile boolean connectionEstablished = true;
////        private boolean alreadyMentionedLost = false;
////        private boolean alreadyMentionedRestored = false;
//        private boolean alreadyMentionedNull = false;
//
//        ScanThread(List<String> type) {
//            this.type = type;
//        }
//
//        @Override
//        public void run() {
//            try {
//                final String fileName = String.format("%s_%s.json",
//                        new SimpleDateFormat("yyyy_MM_dd_hh:mm:ss").format(new Date()),
//                        type != null ? String.join("_", type).toUpperCase() : "all".toUpperCase()
//                );
//
//                this.fileName = fileName;
//                fileWriter.setFileName(fileName);
//
//                Document document = generateDocument("");
//
//                while (document == null) {
//                    if (!alreadyMentionedNull) {
//                        Log.info("Document does not exist, waiting.");
//                        alreadyMentionedNull = true;
//                    }
//                }
//
//                Element element = document.select(".paginate.paginate-dark.pagination-wrapper ul li:last-child a").first();
//                String ref = element.attr("href");
//
//                int minPage = currentPage;
//                int maxPage = maxPages != null ? maxPages : Integer.parseInt(ref.split("/")[2]);
//
//                if (maxPage >= 50) {
//                    Log.warn("More than 50 pages (total: %s). This could take a while!", maxPage);
//                    Thread.sleep(500);
//                }
//
//                Log.info("Scanning first page (%s)", minPage);
//                scanPage(minPage);
//                Log.info("Finished scanning first page (%s)", minPage);
//
//                while (currentPage <= maxPage && !scannedPages.contains(currentPage)) {
//                    int realPage = currentPage;
//                    Log.info("Scanning page %s", currentPage);
//                    scanPage(currentPage);
//                    Log.info("Finished scanning page %s", realPage);
//                }
//
//                Log.info("Writing online servers to file (%s)", new File(fileName).getCanonicalPath());
//                fileWriter.write(onlineServers);
//
//                isFinished = true;
//            } catch (UnirestException | IOException | InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//        private void scanPage(Integer page) throws IOException, UnirestException {
//            try {
//                Log.info("Generated URL: http://minecraft-server-list.com/page/" + page.toString());
//
//                Document document = generateDocument("page/" + page.toString() + "/");
//                while (document == null) {
//                    if (!alreadyMentionedNull) {
//                        Log.info("Document does not exist, waiting.");
//                        alreadyMentionedNull = true;
//                    }
//                }
//
//                Elements elements = document.getElementsByAttributeValue("name", "serverip");
//
//                Log.info("Found %s elements. Scanning.", elements.size());
//
//                scanElements(elements);
//
//                scannedPages.add(page);
//                currentPage++;
//            } catch (ConnectException e) {
//                Log.warn("Couldn't connect to MC Server List.");
//
//                while (retries < 5) {
//                    scanPage(page);
//                    retries++;
//                    Log.info("Trying to connect again...");
//                }
//            }
//        }
//
//        private Document generateDocument(String endpoint) throws IOException {
//            return Jsoup.connect("http://minecraft-server-list.com" + (!endpoint.isEmpty() ? "/" : "") + endpoint)
//                    .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
//                    .header("Upgrade-Insecure-Requests", "1")
//                    .header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36")
//                    .header("Accept-Encoding", "gzip, deflate, sdch")
//                    .header("Cookie", "__utmt=1;")
//                    .get();
//        }
//
////        private void ping() {
////            connectionEstablished = false;
////
////            try {
////                Jsoup.connect("https://google.com").get();
////                connectionEstablished = true;
////            } catch (IOException e) {
////                connectionEstablished = false;
////            }
////        }
//
//        private void scanElements(Elements elements) throws UnirestException, IOException {
//            for (Element e : elements) {
//                String ip = e.val();
//
//                try {
//                    InetAddressValidator validator = new InetAddressValidator();
//                    if (ipsOnly && !validator.isValid(ip)) {
//                        omittedAddresses.add(ip);
//                        continue;
//                    }
//
//                    HttpResponse<JsonNode> response = Unirest.get(String.format("https://us.mc-api.net/v3/server/info/%s/json", ip))
//                            .asJson();
//
//                    if (type == null) Log.info("Found IP address: %s", ip);
//
//                    if (response.getStatus() != 200) {
//                        Log.warn("Failed to get info for IP: " + ip + " (status code: " + response.getStatus() + ")");
//                        continue;
//                    }
//
//                    JsonNode body = response.getBody();
//                    JSONObject object = body.getObject();
//
//                    if ((!object.has("version")) || !(object.getBoolean("online")) || !(object.has("players"))) {
//                        Log.warn("Failed to get version for IP: " + ip + "; probably offline!");
//                        continue;
//                    }
//
//                    JSONObject version = object.getJSONObject("version");
//                    JSONObject players = object.getJSONObject("players");
//                    String versionString = version.getString("name");
//                    Integer onlinePlayers = players.getInt("online");
//                    Integer maxPlayers = players.getInt("max");
//
//                    if (type != null) {
//                        for (String t : type) {
//                            if (matches(t, versionString)) {
//                                Log.info("Found matching IP address: %s", ip);
//                                Log.info("Looks like this server is a " + StringUtils.capitalize(t.toLowerCase()) + " server! Just what you wanted.");
//                                ServerState serverState = new ServerState();
//                                serverState.setIp(ip);
//                                serverState.setPlayers(onlinePlayers);
//                                serverState.setMaxPlayers(maxPlayers);
//                                serverState.setVersion(versionString);
//
//                                onlineServers.add(serverState);
//                            } else {
//                                omittedServers.add(ip);
//                            }
//                        }
//                    } else {
//                        Log.info("No preferred type set, so showing anyway: " + ip);
//                        ServerState serverState = new ServerState();
//                        serverState.setIp(ip);
//                        serverState.setVersion(versionString);
//                        serverState.setPlayers(onlinePlayers);
//                        serverState.setMaxPlayers(maxPlayers);
//
//                        onlineServers.add(serverState);
//                    }
//                } catch (JSONException ex) {
//                    Log.error("An error occurred while trying to parse the JSON response: %s", ex.getMessage());
//                }
//            }
//        }
//        private boolean matches(String type, String version) {
//            switch(type) {
//                case "bungeecord": // BungeeCord servers
//                case "bungee":
//                    return version.matches("(?i).*bungee.*");
//                case "lilypad": // Lilypad (guessing on version string)
//                    return version.matches("(?i).*lily.*");
//                case "spigot": // Spigot servers
//                    return version.matches("(?i).*spigot.*");
//                case "waterfall":
//                    return version.matches("(?i).*waterfall.*");
//            }
//
//            return true;
//        }
//    }
}
