package io.coderleo.scraper.gui;

import io.coderleo.scraper.Log;
import io.coderleo.scraper.ServerState;
import io.coderleo.scraper.api.AbstractScraper;
//import io.coderleo.scraper.api.old_scrapers.MinecraftServersScraper;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.Arrays;

public class GUIAppOld extends Application
{
    private AbstractScraper scraper = new MinecraftServersScraper();
    private volatile boolean finished = false;
    private Button scrapeButton;
    private ComboBox<AbstractScraper> scraperBox;
    private ObservableList<ServerState> servers = FXCollections.observableArrayList();
    private TableView<ServerState> tableView;

    @Override
    public void start(Stage primaryStage) {
        BorderPane border = new BorderPane();

        // HEADER
        HBox header = new HBox();
        header.setPadding(new Insets(20));
        header.setSpacing(10);
        header.setStyle("-fx-background-color: #6e6440;");

        Text text = new Text("POGScraper");
        text.setFont(Font.font("Verdana", 18));
        text.setFill(Color.BLACK);
        text.setStroke(Color.BLACK);

        // Side components
        Region region = new Region();
        HBox.setHgrow(region, Priority.ALWAYS);

        ObservableList<AbstractScraper> options = FXCollections.observableArrayList(new MinecraftServersScraper());
        scraperBox = new ComboBox<>(options);
        scraperBox.setStyle("-fx-border-radius: 0; -fx-background-radius: 0, 0, 0, 0; -fx-background-color: lightgrey; -fx-border-insets: 0; -fx-border-color: transparent;");
        scraperBox.setCellFactory(new Callback<ListView<AbstractScraper>, ListCell<AbstractScraper>>()
        {
            @Override
            public ListCell<AbstractScraper> call(ListView<AbstractScraper> param) {
                return new ListCell<AbstractScraper>()
                {
                    @Override
                    public void updateItem(AbstractScraper item, boolean empty) {
                        super.updateItem(item, empty);
                        setStyle("-fx-border-radius: 0; -fx-background-radius: 0, 0, 0, 0; -fx-border-insets: 0; -fx-border-color: transparent;");

                        if (!empty) {
                            setText(item.getPrettyName());
                            setGraphic(null);
                        } else {
                            setText(null);
                        }
                    }
                };
            }
        });

        scraperBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            Log.info("Selected scraper %s", newValue.getClass().getCanonicalName());
            scraper = newValue;
        });

        scraperBox.setOnMouseClicked(event -> scraperBox.setStyle("-fx-border-radius: 0; -fx-background-radius: 0, 0, 0, 0; -fx-background-color: lightgrey; -fx-border-insets: 0; -fx-border-color: transparent;"));

        scraperBox.getSelectionModel().selectFirst();

        scrapeButton = new Button("Scrape");
        scrapeButton.setStyle("-fx-border-radius: 0; -fx-background-radius: 0, 0, 0, 0; -fx-background-color: white; -fx-border-insets: 0; -fx-border-color: transparent;");
        scrapeButton.setOnMouseClicked(event -> scrape());

        header.getChildren().addAll(text, region, scraperBox, scrapeButton);
        // other
        border.setTop(header);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20));

        tableView = new TableView<>();
        TableColumn<ServerState, String> ipAddressCol = new TableColumn<>("IP Address");
        ipAddressCol.setCellValueFactory(new PropertyValueFactory<>("ip"));

        TableColumn<ServerState, Integer> playersOnlineCol = new TableColumn<>("Players Online");
        playersOnlineCol.setCellValueFactory(new PropertyValueFactory<>("players"));

        TableColumn<ServerState, String> typeCol = new TableColumn<>("Type");
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));

        TableColumn<ServerState, String> versionCol = new TableColumn<>("Version");
        versionCol.setCellValueFactory(new PropertyValueFactory<>("version"));

        tableView.setItems(servers);

        scraper.setServerCallback(server -> {
            servers.add(server);
            tableView.sort();
        });

        ipAddressCol.prefWidthProperty().bind(tableView.widthProperty().divide(4));
        playersOnlineCol.prefWidthProperty().bind(tableView.widthProperty().divide(4));
        typeCol.prefWidthProperty().bind(tableView.widthProperty().divide(4));
        versionCol.prefWidthProperty().bind(tableView.widthProperty().divide(4));

        tableView.setStyle("-fx-focus-color: #848484; -fx-faint-focus-color: #848484;");
        tableView.getColumns().addAll(Arrays.asList(ipAddressCol, playersOnlineCol, typeCol, versionCol));

        GridPane.setHgrow(tableView, Priority.ALWAYS);
        GridPane.setVgrow(tableView, Priority.ALWAYS);

        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        grid.getChildren().add(tableView);

        border.setCenter(grid);

        Scene scene = new Scene(border, 675, 420); // these numbers are not supposed to mean anything.

        primaryStage.setTitle("POGScraper GUI");
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void scrape() {
        finished = false;

        if (scraper == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("");
            alert.setContentText("No scraper was set. Try restarting the application.");

            alert.showAndWait();

            return;
        }

        Log.info("scraping");

        scraperBox.setDisable(true);
        scrapeButton.setDisable(true);
        scrapeButton.setStyle("-fx-border-radius: 0; -fx-background-radius: 0, 0, 0, 0; -fx-background-color: darkgray; -fx-border-insets: 0; -fx-border-color: transparent;");
        scrapeButton.setText("Scraping...");

        new Thread() {
            public void run() {
                scraper.setMaxPage(3);
                scraper.setStartPage(2);

                servers.clear();

                scraper.scrape();

                finished = true;

                Platform.runLater(() -> {
                    Log.info("got here");
                    scraperBox.setDisable(false);
                    scrapeButton.setDisable(false);
                    scrapeButton.setStyle("-fx-border-radius: 0; -fx-background-radius: 0, 0, 0, 0; -fx-background-color: white; -fx-border-insets: 0; -fx-border-color: transparent;");
                    scrapeButton.setText("Scrape");
                });
            }
        }.start();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void nothing() {

    }
}
