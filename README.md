POGScraper
==========

_**DISCLAIMER: I am not responsible for what you do with this!**_

POGScraper is a Minecraft server scraper. Its purpose is to help you find certain types of servers faster, whether to grief or just to have fun on.

Currently it can pull data from one source: [Minecraft Server List](minecraft-server-list.com)
_More sources coming soon!_

Features
--------

* Easy to read console output
* Save online servers to a JSON file
* Only include select types of servers
* Determine how many pages should be scanned before stopping
* Change the page the scraper starts from
* ...and even more!

Building
--------
**You will need to have Maven installed and in your `PATH`, as well as Java 8 or later.**
*If you don't know how to set up Maven, but you have Java, you can get a pre-compiled build by going to the `target` directory of the repository, and grabbing the JAR file _without_ "original" in the beginning of its name*
*Otherwise, you can compile it manually if you so choose. If you're lazy, just get a pre-compiled one.*

1. Clone the repository (well, duh)
2. Make sure you have everything installed properly...
3. Run `mvn clean package` from the root folder of the cloned repository. (The one that has `src/`, `target/`, etc)
4. Your JAR file will be located in `target/POGScrape-1.0-SNAPSHOT.jar`. You can execute it from there.

### Help me! I can't do it!

Well, sadly, I can't help EVERYONE. I can only help people who have done the following things:

* TRY!!!!!!
* have Java properly installed and in their `PATH`
* have Maven properly installed and in their `PATH`
* or know what they're doing, but can't get it to work.


Usage
-----

Running POGScraper is very simple. Here's the most basic command:

`java -jar path-to-pogscraper.jar`

What that command does:

1. Sends a request to [MC Server List](minecraft-server-list.com)
2. Reads the HTML and uses [JSoup](jsoup.org) to parse it.
3. Gets the minimum page (in this case 1, but it can be overwritten)
4. Gets the maximum page by analyzing the pagination links
5. Starts a new thread to scan from the min page to the max page.
6. Once it's done, it will give you a nice ASCII table, as well as a JSON file to look at.
7. You will also get some final statistics.

_Now, if you'd like to get a bit more advanced..._
Here's a nice table with all of the valid options.

| Name     | Requires Value   | Argument Type   | Description   |
| -------- |:----------------:|:---------------:|:-------------:|
| `--type`   | Yes               | String (or list, delimited by ",")         | Specifies what type(s) of servers should be recorded.
| `--start`   | Yes               | Number      | Specifies what page to start from.
| `--max-pages`   | Yes               | Number      | Specifies the maximum page to go to.
| `--ips-only`   | No               | N/A (presence implies yes)      | Specifies that only servers with IPv4 addresses; NOT DOMAINS; should be included.
| `--source`   | Yes               | String      | The fully-qualified classname to instantiate and scrape from. Example: `io.coderleo.scraper.api.scrapers.MinecraftServersScraper`

_There are some other ones, but they are incomplete so I won't show them here._

So, to recap, here's an example command using some of those options.
Say you wanted to get only **BungeeCord** and **Waterfall** servers, from a maximum of 5 pages, starting from page 3...
You could run this command.
`java -jar path-to-pogscraper.jar --type=bungeecord,waterfall --max-pages=5 --start=3`

If you wanted to re-run that, but ONLY get servers back that have IPv4 addresses (not a domain name), you could run:
`java -jar path-to-pogscraper.jar --type=bungeecord,waterfall --max-pages=5 --start=3 --ips-only`

Notice that `--ips-only` does not require a value. Its presence implies that you want that mode toggled.

If you wanted to re-run that AGAIN, but this time only pull servers from `minecraftservers.org`, you could run:
`java -jar path-to-pogscraper.jar --type=bungeecord,waterfall --max-pages=5 --start=3 --ips-only --source="io.coderleo.scraper.api.scrapers.MinecraftServersScraper"`

Roadmap
-------
- Finish "AutoMap" mode, which automatically runs `nmap` for you if you're too tired to do it yourself.
- Improve documentation
- Add a GUI component (probably a whole other project in itself)
- Add support for reading exported data from Shodan (must be provided by the user)

Issues?
-------
If you run into any bugs, please PM me (CoderLeo) on the P.O.G. Discord. I'll try and get back to you ASAP.
Known issues:

* If your connection drops, you'll get a big ugly error that prevents any more scanning. I'm working on fixing that.
* Sometimes the version strings in the generated ASCII table are misaligned. Working on fixing that as well.
* Sometimes some other weird stuff happens. If you get a weird error that doesn't seem to be related to anything else in this list, _please_ PM me and I'll fix it.

Contributing
------------
Right now I don't accept contributions. If you would like a feature to be implemented, PM me on the Discord and I'll see what I can do.
I do plan on accepting contributions _soon_, though. I just need to learn how to do some of the other stuff.